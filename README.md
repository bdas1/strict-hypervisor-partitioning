# strict-hypervisor-partitioning
Strict Partitioning Hypervisor is a project that is aimed to provide a
partitioning scheme to efficiently and securely isolate hardware and software
resources such as CPU, interrupt management, memory, caches, I/O, etc by
leveraging the features supported by Smart NICs (such as MLX Bluefield) and
the kernel through libvirt.
